const defaultTodoList = [
    { text: 'Kegiatan 1', finished: false },
    { text: 'Kegiatan 2', finished: true  },
    { text: 'Kegiatan 3', finished: false },
]

const todos = JSON.parse(localStorage.getItem('todos')) || defaultTodoList

const myStarterPack = {
    todoList: document.getElementById('todo-list'),
    todoInput: document.getElementById('todo-input'),
    addButton: document.getElementById('add-todo'),
    resetButton: document.getElementById('reset-todo'),
}

function renderTodoList() {
    myStarterPack.todoList.innerHTML = null
    todos.forEach((todo, index) => {
        const newTodo = document.createElement('li')
        newTodo.innerText = todo.text
        if(todo.finished){
            newTodo.classList.add('list-group-item')
        } else {
            newTodo.classList.add('list-group-item')
            const finishButton = document.createElement('button')
            finishButton.classList.add('btn','btn-success')
            finishButton.innerText = 'Click as finished'
            finishButton.addEventListener('click', () => finishTodo(index))
            newTodo.append(' => ',finishButton)
        }

        const deleteButton = document.createElement('button')
        deleteButton.classList.add('btn','btn-danger')
        deleteButton.innerText = 'delete'
        deleteButton.addEventListener('click', () => deleteTodo(index))
        newTodo.append(' => ',deleteButton)

        myStarterPack.todoList.append(newTodo)
    })
}

function storeAndRender() {
    localStorage.setItem('todos', JSON.stringify(todos))
    renderTodoList()
}

function isInputFilled() {
    return myStarterPack.todoInput.value.length > 0
}

function addTodo() {
    if (isInputFilled()) {
      const todoText = myStarterPack.todoInput.value
      todos.push({ text: todoText, finished: false })
      storeAndRender()
      myStarterPack.todoInput.value = ''
      myStarterPack.todoInput.focus()
    }
  }

function finishTodo(index) {
    todos[index].finished = true
    storeAndRender()
}
  
function deleteTodo(index) {
    todos.splice(index, 1)
    storeAndRender()
}

myStarterPack.todoInput.addEventListener('keypress', e =>
  e.keyCode === 13 ? addTodo() : {}
)

myStarterPack.addButton.addEventListener('click', () => addTodo())

myStarterPack.resetButton.addEventListener('click', () => {
  localStorage.clear()
  window.location.reload()
})  

storeAndRender()
myStarterPack.todoInput.focus()